export default function maxMap (array, mappingFunc) {
  // TODO:
  //   This function will find the maximum mapped value of `array`. The mapped value
  //   for each item should be calculated using `mappingFunc`.
  //
  //   Please read the test to get a basic idea.
  // <-start-
  if (!array) {
    return undefined;
  }

  const arrayMapped = array.map(mappingFunc);
  let maximumInitiated = false;
  let index = 0;

  while (!maximumInitiated && index < arrayMapped.length) {
    if (arrayMapped[index] || arrayMapped[index] === 0) {
      var maximumValue = arrayMapped[index];
      maximumInitiated = true;
    }
    index = index + 1;
  }

  for (let j = index; j < arrayMapped.length; j++) {
    if ((arrayMapped[j] || arrayMapped[j] === 0) && arrayMapped[j] > maximumValue) {
      maximumValue = arrayMapped[j];
    }
  }

  return maximumValue;
  // --end-->
}
