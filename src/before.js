export default function before (n, func) {
  // TODO:
  //   Creates a function that invokes func while it's called less than `n` times.
  //   Please read the test to get how it works.
  // <-start-
  if (isNaN(n) || n === 0) {
    return () => 0;
  }
  var count = 0;
  return function () {
    count = count + 1;
    if (count >= n) {
      return () => n - 1;
    }
    return func();
  };
  // --end-->
}
